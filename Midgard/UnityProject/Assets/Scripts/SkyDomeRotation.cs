using UnityEngine;
using System.Collections;

public class SkyDomeRotation : MonoBehaviour 
{
    public float spinXSpeed;
    public float spinYSpeed;
    public float spinZSpeed;
    
    void Update () 
    {
        transform.Rotate(spinXSpeed, spinYSpeed, spinZSpeed);
    }
}
