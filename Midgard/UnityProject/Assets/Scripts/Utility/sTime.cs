﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class sTime : MonoBehaviour
{
    public float TimeModifier = 1.0f;
    private float _deltaTime;
    static public float deltaTime
    {
        get { return _sTime._deltaTime; }
    }
    static public string timeStamp
    {
        get
        {
            return System.DateTime.Now.ToString();            
        }
    }
    public static sTime _sTime;
    
    public void Awake()
    {
        _sTime = this;
    }
    public void Update()
    {
        _deltaTime = Time.deltaTime*TimeModifier;
    }
}