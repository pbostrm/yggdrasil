using UnityEngine;
using System.Collections;

public class RotateAroundObject : sBehaviour
{
    public Transform CenterObject;

    public Vector3 rotationAxle;
    public float relativeHeading = 90;
    public float AnglesPerSecond = 180.0f;

    private Vector3 towardsPlanetVector;
    private Vector3 directionVector;
    private Vector3 newDirectionVector;
	// Use this for initialization
	void Start ()
	{
        towardsPlanetVector = transform.position - CenterObject.position;

        directionVector = Vector3.Cross(towardsPlanetVector, Vector3.up);
        newDirectionVector = RotateAroundPoint(directionVector, Vector3.zero,
                                               Quaternion.AngleAxis(relativeHeading, towardsPlanetVector));
	}
	
	// Update is called once per frame

    public override void sDrawGizmos()
    {
        
        
        if (towardsPlanetVector != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(CenterObject.position,CenterObject.position+towardsPlanetVector);
        }
        if (directionVector != null)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(CenterObject.position, CenterObject.position + directionVector);
        }
        if (newDirectionVector != null)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(CenterObject.position, CenterObject.position + newDirectionVector);
        }
    }
	void Update () 
    {
        if (CenterObject != null)
        {
            
            rotationAxle = Vector3.Cross(towardsPlanetVector, newDirectionVector);

            if (CenterObject == transform.parent)
            {
                transform.localPosition = RotateAroundPoint(transform.localPosition, Vector3.zero,
                                                            Quaternion.AngleAxis(relativeHeading, towardsPlanetVector));

            }
            else
            {
                transform.RotateAround(CenterObject.position, rotationAxle, AnglesPerSecond * Time.deltaTime);
                
            }
            
            transform.eulerAngles = Vector3.zero;


        }
	}
    static Vector3 RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle)
    {
        return angle * (point - pivot) + pivot;

    }
}
