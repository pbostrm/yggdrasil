using UnityEngine;
using System.Collections;

public class CelestialSelector : MonoBehaviour
{
    private Camera cam;
    public LayerMask celestialLayers;
    public CameraRotate cameraPivot;

    public Vector3 gizRaystart;
    public float gizRaylength;
    public Vector3 gizRaydirection;
	// Use this for initialization
	void Start () 
    {
        if (cam == null)
        {
            cam = Camera.main;
        }
        if (cameraPivot == null)
        {
            cameraPivot = gameObject.GetComponentInChildren<CameraRotate>();
            if (cameraPivot == null)
            {
                Debug.Log("bajs!");
            }
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Debug.Log("Oh Shit!");
            SelectCelestial();
        }

	}
    public void SelectCelestial()
    {
        Ray ray = Camera.mainCamera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));

        RaycastHit hit;
        //DebugOutput.Shout(ray.origin.ToString()+"  " + ray.direction.ToString());

        gizRaydirection = ray.direction;
        gizRaystart = ray.origin;
        gizRaylength = 1000.0f;
        if (Physics.Raycast(ray, out hit, 1000.0f, celestialLayers))
        {
            //Debug.Log("OHSHAIT WE HIT STUFF!!");
            //transform.position = hit.collider.transform.position + new Vector3(0, 0, 7);
            transform.parent = hit.collider.transform;
            transform.localEulerAngles = Vector3.zero;
            transform.localPosition = Vector3.zero;
            cameraPivot.transform.localEulerAngles = Vector3.zero;
            cameraPivot.zoomStep = 4;
            //transform.LookAt(hit.collider.transform);
            if (transform.parent != null)
            {
                RotateAroundObject rao = transform.parent.GetComponent<RotateAroundObject>();
                if (rao != null)
                {

                    Vector3 v1 = transform.parent.position;
                    //transform.parent.RotateAround(rao.CenterObject.position, rao.rotationAxle,15);
                    Vector3 v2 = transform.parent.position;
                    //transform.parent.RotateAround(rao.CenterObject.position, rao.rotationAxle,-15);

                    Vector3 v3 = v1 + (v2 - v1).normalized*15.0f;
                    //transform.localPosition = (v3 - rao.CenterObject.position).normalized*CameraRotate.zoomStep*2;

                    //transform.RotateAroundLocal();
                    //transform.LookAt((transform.parent.position+rao.CenterObject.position)*0.5f);
                }
            }
            
            cameraPivot.transform.LookAt(transform);
        }
    }
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(gizRaystart, gizRaystart + (gizRaydirection * gizRaylength));

        if (transform.parent != null)
        {
            
            RotateAroundObject rao = transform.parent.GetComponent<RotateAroundObject>();
            if (rao != null)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position, transform.parent.position);
                Gizmos.DrawLine(transform.position, rao.CenterObject.position);
                Gizmos.DrawLine(rao.CenterObject.position, transform.parent.position);
            }
        }
    }
}
