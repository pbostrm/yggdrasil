﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class GUITestLab : MonoBehaviour
{
    public void Awake()
    {
        ActiveGUI.activeGUI.AddActiveGUIObject("Testing/Test123");
        ActiveGUI.activeGUI.AddActiveGUIObject("Testing/Testabc");
        ActiveGUI.activeGUI.AddActiveGUIObject("Testing/Testabc/lalal/banan/kaka");
        ActiveGUI.activeGUI.AddActiveGUIObject("Testing/Testabc/lolol");
        ActiveGUI.activeGUI.AddActiveGUIObject("Testing/Testabc/lolol/traffle/lol");
        ActiveGUI.activeGUI.AddActiveGUIObject("Testing/Testabc/lolol");
        ActiveGUI.activeGUI.AddActiveGUIObject("Testing/Testabc/lolol");
        ActiveGUI.activeGUI.AddActiveGUIObject("LOLOL/Test123");
        ActiveGUI.activeGUI.AddActiveGUIObject("apfar/Test123");
        ActiveGUI.activeGUI.AddActiveGUIObject("LaunchTheMissile!/Test123");
        ActiveGUI.activeGUI.AddActiveGUIObject("LaunchTheMissile!/kalle");
        ActiveGUI.activeGUI.AddActiveGUIObject("LaunchTheMissile!/balle");
        ActiveGUI.activeGUI.AddActiveGUIObject("LaunchTheMissile!/fikon");
        ActiveGUI.activeGUI.AddActiveGUIObject("LaunchTheMissile!/skalle");
    }

}
