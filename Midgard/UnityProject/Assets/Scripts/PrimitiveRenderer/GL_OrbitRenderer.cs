﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class GL_OrbitRenderer : GL_Object
{
    public Vector3 AroundAxle;
    public int Resolution = 180;
    public float Radius = 2.0f;
    float x
    {
        get { return transform.position.x; }
    }
    float y
    {
        get { return transform.position.y; }
    }
    float z
    {
        get { return transform.position.z; }
    }
    public Color color;
    private RotateAroundObject RAO;
    public void Awake()
    {
        if (RAO == null)
        {
            RAO = GetComponent<RotateAroundObject>();
            if (RAO == null)
            {
                Debug.Log("YOU are out of luck");
            }
        }
        GL_LineRenderer.GL_Objects.Add(this);
    }
    public override void GL_Draw()
    {
        GL.Color(color);
        Vector3 startPoint = transform.position;
        Vector3 LinePoint = startPoint;
        Vector3 lastLinePoint = startPoint;
        for (int i = 0; i <= Resolution; i++)
        {
            lastLinePoint = LinePoint;
            LinePoint = RotateAroundPoint(startPoint, RAO.CenterObject.position,
                                              Quaternion.AngleAxis(2* i, RAO.rotationAxle));

            GL.Vertex3(lastLinePoint.x, lastLinePoint.y, lastLinePoint.z);

            //GL.Vertex3(LinePoint.x, LinePoint.y, LinePoint.y);
            GL.Vertex3(LinePoint.x, LinePoint.y, LinePoint.z);

            //GL.Vertex3(LinePoint);
            //GL.Vertex3(x, y, z);
            if (i == 2)
            {
                //Debug.Log("e " + lastLinePoint+" "+ LinePoint);
            }
        }



        //GL.Vertex3(x+Radius, y, z);


    }
    public void OnDrawGizmos()
    {
        if (RAO == null)
        {
            return;
        }
        Gizmos.color = color;
        Vector3 startPoint = transform.position;
        Vector3 LinePoint = startPoint;
        Vector3 lastLinePoint = startPoint;
        for (int i = 0; i <= Resolution; i++)
        {
            lastLinePoint = LinePoint;
            LinePoint = RotateAroundPoint(startPoint, RAO.CenterObject.position,
                                              Quaternion.AngleAxis(2 * i, RAO.rotationAxle));


            Gizmos.DrawLine(lastLinePoint,LinePoint);

        }
    }
    static Vector3 RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle)
    {
        return angle * (point - pivot) + pivot;

    }
}
