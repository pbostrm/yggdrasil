﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BifrostCore
{
    public class DebugOutput
    {
        static DebugOutput _debugOutput;
        static DebugOutput debugOutput
        {
            get
            {
                if (_debugOutput == null)
                {
                    _debugOutput = new DebugOutput();
                }
                return _debugOutput;
            }
        }
        public DebugOutput()
        {
            if (_debugOutput == null)
            {
                _debugOutput = this;
            }
        }

        static public void Shout(string s)
        {
            Console.WriteLine(s);
        }
    }
}
